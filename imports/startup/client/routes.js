import React, { Component } from 'react'
import { Router, Route, Switch } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import Home from '../../ui/pages/Home.js'
import Register from '../../ui/pages/Register'
import UpToOneMonth from '../../ui/pages/evaluationForm/UpToOneMonth'
import OneMonthToTwoMonth from '../../ui/pages/evaluationForm/OneMonthToTwoMonth'

const browserHistory = createBrowserHistory();

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/evaluation-form/0-1-month" component={UpToOneMonth} />
      <Route exact path="/evaluation-form/1-2-month" component={OneMonthToTwoMonth} />
    </Switch>
  </Router>
)