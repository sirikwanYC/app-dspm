import axios from 'axios'
import React, { Component } from "react"
import moment from 'moment'
import 'moment/locale/th'
import { Radio, Input, Icon, message } from "antd"
import ModalInfo from './ModalInfo'
import ModalAssessor from './ModalAssessor'


export default class Content extends Component {
  state = {
    visible: false,
    index: 0,
    play: false,
    date: [],
    openImage: false,
    srcImg: ''
  }

  onClickPlaySound = (index) => {
    const url = '../../public/sound/question1.mp3'
    this.audio = new Audio(url)
    this.setState({
      play: true,
    })
    console.log(this.audio);
    this.audio.play();
  }

  setDate = () => {
    this.setState({
      date: this.state.date.push('')
    })
  }

  onChangeRadio = (event, index, no, questionType) => {
    const date = this.state.date
    date[index] = moment(new Date()).format('ll')
    this.setState({
      date
    })
    const urlInsertEvaluationForm = `http://localhost:4000/insert-evaluation-form`
    axios.post(urlInsertEvaluationForm, {
      cid: this.props.cid,
      spanOfAge: this.props.spanOfAge,
      no,
      questionType,
      result: event.target.value,
    }).then(() => {
      this.props.getEvaluationResult()
      message.success('บันทึกเรียบร้อย')
    })
  }

  showModal = (event, index) => {
    this.setState({
      visible: true,
      index
    })
  }

  handleCancel = (e) => {
    this.setState({
      visible: false,
    })
  }

  setDefaultDate = (index) => {
    const arrayResult = []
    this.props.result.forEach((value, index) => {
      arrayResult.push(value)
    })

    let answer

    arrayResult.forEach((value, i) => {
      if (index + 1 === value.question_no) {
        answer = moment(value.assessment_date).format('ll')
        arrayResult.pop(arrayResult[i])
      }
    })
    if (answer === undefined) {
      answer = this.state.date[index]
    }
    return answer
  }

  setDefaultChecked = (index, result) => {
    const arrayResult = []
    this.props.result.forEach((value) => {
      arrayResult.push(value)
    })

    let answer

    arrayResult.forEach((value, i) => {
      if (index + 1 === value.question_no && value.result === result) {
        answer = true
        arrayResult.pop(arrayResult[i])
      }
    })
    if (answer === undefined) {
      answer = false
    }
    return answer
  }

  showImage = (src) => {
    this.setState({
      openImage: !this.state.openImage,
      srcImg: src
    })
  }

  render() {
    return (
      <div className="font font-small font-weight-heavy" style={this.props.openMenu ? { position: "fixed", marginTop: "21%" } : { marginTop: "21%" }}>
        {this.state.openImage ?
          <div className="mask-open-img" onClick={() => this.showImage('')}>
              <img src={this.state.srcImg} width="800px" />
          </div>
          : ''
        }
        {this.props.content.map((value, index) => {
          () => this.setDate()
          return (
            <div key={`content${+index}`}>
              <div className="content">
                <div className="content-skill font-medium">
                  <div className="sound-icon">
                    <Icon onClick={() => this.onClickPlaySound(index)} type="sound" theme="twoTone" />
                  </div>
                  <div className="content-skill-left">
                    <p> {index + 1}. </p>
                  </div>
                  <div className="content-skill-right">
                    <div className="question-skill">
                      <p> {value.question} </p>
                      {
                        value.imagesTop && <div>
                          <img
                            className="magin-img-skill"
                            src={value.imagesTop.src}
                            width={value.imagesTop.width}
                            onClick={() => this.showImage(value.imagesTop.src)}
                          />
                        </div>
                      }
                      {
                        value.accessories && <p className="margin-top-accessories"> {value.accessories} </p>
                      }
                      {
                        value.imagesBottom && <img
                          className="magin-img-skill"
                          src={value.imagesBottom.src}
                          width={value.imagesBottom.width}
                          onClick={() => this.showImage(value.imagesBottom.src)}
                        />
                      }
                      {value.note && <p> {value.note} </p>}
                    </div>
                    <div className="content-footer font-small">
                      <div className="icon-youtube">
                        <a href={value.urlVideo}><Icon type="youtube" /> ดูวิดีโอ </a>
                      </div>
                      <div className="icon-info">
                        <ModalInfo index={index} contentModal={this.props.contentModal} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="content-no">
                  <div className="content-no-on">
                    <Radio.Group onChange={(e) => this.onChangeRadio(e, index, value.no, value.questionType)}>
                      <Radio className="font green-color box-radio padding-buttom-on" value="ผ่าน" checked={this.setDefaultChecked(index, 'ผ่าน')}> ผ่าน </Radio>
                      <Radio className="font red-color box-radio" value="ไม่ผ่าน" checked={this.setDefaultChecked(index, 'ไม่ผ่าน')}> ไม่ผ่าน </Radio>
                    </Radio.Group>
                  </div>
                  <div>
                    <Input defaultValue={this.props.result.length !== 0 ? this.setDefaultDate(index) : ''} className="font" />
                  </div>
                  <div >
                    <ModalAssessor
                      index={index + 1}
                      questionNo={value.no}
                      cid={this.props.cid}
                      spanOfAge={this.props.spanOfAge}
                      result={this.props.result}
                      getEvaluationResult={this.props.getEvaluationResult}
                    />
                  </div>
                </div>
              </div>
              <hr className="separator-no" />
            </div>
          )
        })}
      </div>
    )
  }
}
