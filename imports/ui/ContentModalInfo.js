import React, { Component } from 'react'

export default class ContentModalInfo extends Component {
    constructor(props) {
        super()
    }

    render() {
        return (
            <div className="content-modal">
                <div className="evaluation">
                    <p className="black-color" >วิธีการประเมิน และเฝ้าระวัง โดยพ่อแม่ ผู้ปกครอง และเจ้าหน้าที่</p>
                    {
                        this.props.contentModal[this.props.index].evaluation.text.map((value, index) => <p key={`evaluation${+index}`}> {value} </p>)
                    }
                    <p className="blue-color" >ผ่าน: {this.props.contentModal[this.props.index].evaluation.complete} </p>
                </div>
                <div className="practice-skills">
                    <p className="black-color" >วิธีฝึกทักษะ โดยพ่อแม่ ผู้ปกครอง และเจ้าหน้าที่</p>
                    {
                        this.props.contentModal[this.props.index].practiceSkills &&
                        this.props.contentModal[this.props.index].practiceSkills.text.map((value, index) => <p key={`practiceSkills${+index}`}> {value} </p>)
                    }
                    {
                        this.props.contentModal[this.props.index].practiceSkills.replacementToys &&
                        <p className="red-color">ของเล่นที่ใช้แทนได้:  {this.props.contentModal[this.props.index].practiceSkills.replacementToys} </p>
                    }
                    {
                        this.props.contentModal[this.props.index].practiceSkills.purpose &&
                        <p className="green-color">วัตถุประสงค์:  {this.props.contentModal[this.props.index].practiceSkills.purpose} </p>
                    }
                </div>
            </div>
        )
    }
}