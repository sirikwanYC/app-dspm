import React, { Component } from 'react'
import { Input, Form } from 'antd';

class ContentAssessorForm extends Component {
    setDefaultAssessor = (index) => {
        const arrayResult = []
          this.props.result.forEach((value, index) => {
            arrayResult.push(value)
          })
          
          let answer
    
          arrayResult.forEach((value, i)=> {
            if(index === value.question_no){
              answer=value.assessor
              arrayResult.pop(arrayResult[i])
            }
          }) 
          if(answer === undefined) {
            answer = ''
          }
          return answer
      }

      setDefaultHopsCode = (index) => {
        const arrayResult = []
          this.props.result.forEach((value, index) => {
            arrayResult.push(value)
          })
          
          let answer
    
          arrayResult.forEach((value, i)=> {
            if(index === value.question_no){
              answer=value.hosp_code
              arrayResult.pop(arrayResult[i])
            }
          }) 
          if(answer === undefined) {
            answer = ''
          }
          return answer
      }
    render() {
        return (
            <div className="content-assessor">
                <div className="content">
                    <div className="left">
                        <p>ผู้ประเมิน</p>
                    </div>
                    <div className="right">
                        <Form.Item>
                            {this.props.form.getFieldDecorator(`assessor${this.props.index}`, {
                                rules: [{ required: true, message: 'กรุณากรอกผู้ประเมิน' }],
                                initialValue: this.setDefaultAssessor(this.props.index)
                            })(
                                <Input className="font font-small" />)}
                        </Form.Item>
                    </div>
                </div>
                <div className="content">
                    <div className="left">
                        <p>รหัสโรงพยาบาล</p>
                    </div>
                    <div className="right">
                        <Form.Item>
                            {this.props.form.getFieldDecorator(`hopsCode${this.props.index}`, {
                                rules: [{ required: true, message: 'กรุณากรอกรหัสโรงพยาบาล' }],
                                initialValue: this.setDefaultHopsCode(this.props.index)
                            })(
                                <Input className="font font-small" />
                            )}
                        </Form.Item>
                    </div>
                </div>
            </div>
        )
    }
}

const ContentAssessor = Form.create()(ContentAssessorForm)

export default ContentAssessor