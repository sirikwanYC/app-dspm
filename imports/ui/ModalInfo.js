import React, { Component } from 'react'
import { Modal, Icon } from "antd"
import ContentModalInfo from './ContentModalInfo'


export default class ModalInfo extends Component {
    constructor (props) {
        super()
    }

    state = {
        visible: false
    }

    showModal = () => {
        this.setState({
            visible: true,
        })
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        })
    }

    render() {
        return (
            <div>
                <p onClick={this.showModal}><Icon type="info-circle" /> ทักษะ </p>
                <Modal
                    title="วิธีประเมิน และวิธีฝึกทักษะ"
                    id={`from${this.props.questionNo}`}
                    centered
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    footer={null}
                    width="90%"
                    className="font"
                >
                    <ContentModalInfo index={this.props.index}  contentModal={this.props.contentModal}/>
                </Modal>
            </div>
        )
    }
  }
