import { Icon, Button } from 'antd'
import React, { Component } from 'react'


export default class Layout extends Component {
  constructor(prop) {
    super()
  }
  state = {
    collapsed: false,
    link: [
      { age: '0 - 1 ปี', text: 'แรกเกิด - 1 เดือน', href: 'evaluation-form/0-1-month' },
      { text: '1 - 2 เดือน', href: 'evaluation-form/1-2-month' },
      { text: '3 - 4 เดือน', href: '' },
      { text: '5 - 6 เดือน', href: '' },
      { text: '7 - 6 เดือน', href: '' },
      { text: '9 เดือน', href: '' },
      { text: '10 - 12 เดือน (10 เดือน - 1 ปี)', href: '' },
      { age: '1 - 2 ปี', text: '13 - 15 เดือน (1 ปี 1 เดือน - 1 ปี 3 เดือน)', href: '' },
      { text: '16 - 17 เดือน (1 ปี 4 เดือน - 1 ปี 5 เดือน)', href: '' },
      { text: '18 เดือน (1 ปี 6 เดือน)', href: '' },
      { text: '19 - 24 เดือน (1 ปี 7 เดือน - 2 ปี)', href: '' },
      { age: '2 - 3 ปี', text: '25 - 29 เดือน (2 ปี 1 เดือน - 2 ปี 5 เดือน)', href: '' },
      { text: '30 เดือน (2 ปี 6 เดือน)', href: '' },
      { text: '31 - 36 เดือน (2 ปี 7 เดือน - 3 ปี)', href: '' },
      { age: '3 - 4 ปี', text: '37 - 41 เดือน (3 ปี 1 เดือน - 3 ปี 5 เดือน)', href: '' },
      { text: '42 เดือน (3 ปี 6 เดือน)', href: '' },
      { text: '43 - 48 เดือน (3 ปี 7 เดือน - 4 ปี)', href: '' },
      { age: '4 - 5 ปี', text: '49 - 54 เดือน (4 ปี 1 เดือน - 6 เดือน)', href: '' },
      { text: '55 - 60 เดือน (4 ปี 7 เดือน - 5 ปี)', href: '' }
    ]
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    })
    this.props.openMenu(!this.state.collapsed)
  }

  render() {
    return (
      <div className="nav-head" style={this.state.collapsed ? { position: 'absolute', marginTop: 0 } : {}}>
        <div className="hamberger-menu white-color font-medium" >
          <Button type="default" onClick={this.toggleCollapsed} style={{ marginBottom: 16, height: 90 }} className={this.state.collapsed ? 'hamberger-menu-open' : ''}>
            <Icon type={this.state.collapsed ? 'menu-fold' : 'menu-unfold'} className="font-small" style={{ fontSize: '60px' }} />
          </Button>

        </div>
        <div className="nav-content">
          <h1 className="font white-color font-medium font-weight-heavy" > {this.props.textHead} </h1>
          <p className="font white-color font-weight-heavy" > คุณแม่ควรประเมินให้เสร็จก่อน 24 ธ.ค. 61 </p>
        </div>
        {
          this.state.collapsed ?
            <div className="mask-open-menu" onClick={this.toggleCollapsed}>
              <div className="open-menu font font-small" >
                <p className="font-medium font-weight-heavy"> ช่วงอายุ </p>
                <hr className="separator-head" />
                <div className="open-book-pink">
                  <div className="link-book-pink font-weight-heavy">
                    <a href="/"><Icon type="link" /> เปิดเล่มชมพู</a>
                  </div>
                </div>
                {
                  this.state.link.map((value, index) => {
                    return (
                      <div key={+index} className="tag-a-menu">
                        {value.age &&
                          <p className="font-medium font-weight-heavy" style={{ marginBottom: "2%" }}><Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" /> {value.age} </p>
                        }
                        <div className="link-menu">
                          <div className="cicle-icon">
                            <div className="cicle"></div>
                          </div>
                          <div className="link">
                            <a href={`/${value.href}`}>{value.text}</a>
                          </div>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
            : ''
        }
      </div>
    )
  }
}