import { Icon } from "antd"
import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import Layout from '../Layout'

export default class Home extends Component {
  state = {
    loading: false
  }

  componentWillMount = () => {
    setTimeout(() => {
      this.setState({
        loading: true
      })
    }
    , 3000)
  }

  render() {
    const { loading } = this.state
    return (
      <div className="home">
        {loading ? <Redirect to="register" /> : ''}
        <Layout>
        <div className="loading"> <Icon type="loading" /> </div>
          <div className="box-logo-head">
            <div className="logo-head">
              <img
                src="/images/5.png"
                width="100%"
              />
            </div>
          </div>
          <div className="box-cloud-one">
            <div className="cloud-one">
              <img
                src="/images/2.png"
                width="100%"
              />
            </div>
          </div>
          <div className="box-header">
            <img
              src="/images/4.png"
              width="90%"
            />
          </div>
          <div className="box-cloud-two">
            <div className="cloud-two">
              <img
                src="/images/3.png"
                width="100%"
              />
            </div>
          </div>
          <div className="box-content">
            <img
              src="/images/1.png"
              width="90%"
            />
          </div>
          <div className="box-logo-footer">
            <div className="logo-one">
              <img
                src="/images/logo_MOPH.png"
                width="60%"
              />
            </div>
            <div className="logo-two">
              <img
                src="/images/logo_nhso.jpg"
                width="80%"
              />
            </div>
          </div>
          <div className="box-footer">
            <img
              src="/images/6.png"
              width="100%"
            />
          </div>
        </Layout>
      </div>
    )
  }
}