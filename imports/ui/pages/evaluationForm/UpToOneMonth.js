import React, { Component } from 'react'
import axios from 'axios';
import Navbar from '../../Navbar'
import Layout from '../../Layout'
import Content from '../../Content'
import {content, contentModal} from '../../../constant/UpToOneMonth'

export default class UpToOneMonth extends Component {
  componentWillMount = () => {
    this.getEvaluationResult()
  }
  state = {
    openMenu: false,
    result: [],
    cid: localStorage.getItem('cid')
  }
  openMenu = () => {
    this.setState({
      openMenu: !this.state.openMenu
    })
  }
  getEvaluationResult = async() => {
    const urlGetEvaluationResult = `http://localhost:4000/evaluation-result/${this.state.cid}/span-of-age/0-1`
    await axios.get(urlGetEvaluationResult)
    .then((result) => {
      this.setState({
        result: result.data
      })
    })
  }
  render() {
    const {openMenu} = this.state
    return (
      <div className="container">
        <Layout>
            <Navbar openMenu={this.openMenu} textHead="แรกเกิด - 1 เดือน"/>
            <Content 
              openMenu={openMenu} 
              content={content}
              contentModal={contentModal}
              spanOfAge="0-1"
              cid={this.state.cid}
              result={this.state.result}
              getEvaluationResult={this.getEvaluationResult}
              />
        </Layout>
      </div>
    )
  }
}