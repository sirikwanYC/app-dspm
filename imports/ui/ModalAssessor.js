import React, { Component } from 'react'
import axios from 'axios'
import { Modal, Icon, Button, Form, message } from "antd"
import ContentAssessor from './ContentAssessor'


class WrappedModalAssessor extends Component {
    constructor(props) {
        super()
    }

    state = {
        visible: false
    }

    showModal = () => {
        this.setState({
            visible: true,
        })
    }

    insertAssessor = () => {
        const urlInsertEvaluationForm = `http://localhost:4000/insert-evaluation-form`
        axios.post(urlInsertEvaluationForm, {
            cid: this.props.cid,
            spanOfAge: this.props.spanOfAge,
            no: this.props.questionNo,
            assessor: this.props.form.getFieldValue(`assessor${this.props.index}`),
            hospCode: this.props.form.getFieldValue(`hopsCode${this.props.index}`)
        }).then(() => {
            this.setState({
                visible: false,
            })
            this.props.getEvaluationResult()
            message.success('บันทึกเรียบร้อย')
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFields((err) => {
            if (!err) {
                this.insertAssessor()
            }
        })
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        })
    }

    render() {
        return (
            <div className="modal-assessor">
                <Button onClick={this.showModal} className="font-small font-weight-heavy"> ผู้ประเมิน </Button>
                <Form id={`fromAssessor${this.props.index}`} onSubmit={this.handleSubmit}>
                    <Modal
                        title="ผู้ประเมิน"
                        centered
                        onCancel={this.handleCancel}
                        visible={this.state.visible}
                        width="90%"
                        className="font"
                        footer={[
                            <Form.Item key="button">
                                <Button
                                    onClick={this.handleCancel}
                                    key="cancel"
                                > ยกเลิก
                                </Button>
                                <Button style={{ backgroundColor: '#52c41a', color: '#fff' }}
                                    form={`fromAssessor${this.props.index}`}
                                    htmlType="submit"
                                > ตกลง
                                </Button>
                            </Form.Item>
                        ]}
                    >
                        <ContentAssessor
                            index={this.props.index}
                            form={this.props.form}
                            result={this.props.result}
                        />
                    </Modal>
                </Form>
            </div>
        )
    }
}

const ModalAssessor = Form.create()(WrappedModalAssessor)

export default ModalAssessor